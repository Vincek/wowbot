import pyautogui
import time
import random
from controls import *
from utils import *
import threading

class Action:

    def __init__(self):
        self.duration = 1 # the time to reserve for this action after starting it
        self.probability = 1 # the chance of this action to occur when present in the action list
        self.priority = 1 # the lower the better

    def doAction(self,options,state):
        pass

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        pass

    @classmethod
    def getLastTimeUsed(cls):
        pass



class ChillAction(Action):
    cooldown = 0
    lastTimeUsed = 0

    def __init__(self, duration):
        super().__init__()
        self.duration = duration

    def doAction(self,options,state):
        time.sleep(self.duration)
        print("Chilling")

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed
        



class CancelTargetAction(Action):
    cooldown = 5
    lastTimeUsed = 0

    def __init__(self, probability):
        super().__init__()
        self.probability = probability

    def doAction(self,options,state):
        k = PyKeyboard()
        pressKeyLikeHuman(k.escape_key)

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed



class AutoAttackAction(Action):
    
    cooldown = 20
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        self.duration = 0

    def doAction(self,options,state):
        pressKeyLikeHuman(options.keys["autoAttack"])
        print("Auto attack!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class StrikeAttackAction(Action):

    cooldown = 4
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        self.duration = 0

    def doAction(self,options,state):
        pressKeyLikeHuman(options.keys["strike"])
        print("Auto attack!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class StrikeCloseAttackAction(Action):

    cooldown = 4
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        self.duration = 0

    def doAction(self,options,state):
        pressKeyLikeHuman(options.keys["close"])
        print("Close attack!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class DotAttackAction(Action):

    cooldown = 10
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        self.duration = 0

    def doAction(self,options,state):
        pressKeyLikeHuman(options.keys["dot"])
        print("Dotting!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class PetAttackAction(Action):

    cooldown = 1
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        self.duration = 0

    def doAction(self,options,state):
        k.tap_key(k.numpad_keys[3])
        print("Sending pet!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class JumpAction(Action):

    cooldown = 0
    lastTimeUsed = 0

    def __init__(self,prob=1):
        super().__init__()
        self.probability = prob

    def doAction(self,options,state):
        pressKeyLikeHuman(options.keys["jump"])
        print("jump!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class LootAction(Action):

    cooldown = 0
    lastTimeUsed = 0

    def __init__(self,prob=1):
        super().__init__()
        self.probability = prob

    def doAction(self,options,state):
        # k = PyKeyboard()
        # holdKeyLikeHuman(k.shift_l_key)
        m = PyMouse()
        res = pyautogui.size()
        center = (res[0]/2,res[1]/2)
        # m.click(int(center[0])+random.randint(-100,100),int(center[1])+random.randint(-100,100),2)
        pyautogui.moveTo(int(center[0])+random.randint(-100,100),int(center[1])+random.randint(-100,100))
        pyautogui.mouseDown(button='right')
        time.sleep(random.random()*0.1)
        pyautogui.mouseUp(button='right')
        # releaseKeyLikeHuman(k.shift_l_key)
        print("loot!")\

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed
    



class WalkAction(Action):

    cooldown = 3
    lastTimeUsed =0

    def __init__(self,prob):
        super().__init__()
        self.probability = prob
        
        
    def doAction(self,options,state):
        holdKeyLikeHuman(options.keys["forward"])
        time.sleep(random.random()+1)
        releaseKeyLikeHuman(options.keys["forward"])

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed



class WalkToTargetAction(Action):

    cooldown = 0.2
    lastTimeUsed = 0

    def __init__(self,targetLocation):
        super().__init__()
        self.duration = 0
        self.targetLocation = targetLocation
        

    def doAction(self,options,state):
        print("walking towards target")
        #print("target location for walking %d %d" % self.targetLocation)
        if(self.targetLocation[1]>options.resolution[1]/2-options.resolution[1]/20): # target is close
            releaseKeyLikeHuman(options.keys["forward"])
            releaseKeyLikeHuman(options.keys["backward"])
            print("stopping")

        if(self.targetLocation[1]<options.resolution[1]/2-options.resolution[1]/20): # target is far
            holdKeyLikeHuman(options.keys["forward"])
            releaseKeyLikeHuman(options.keys["backward"])
            print("walking forward")

        if(self.targetLocation[1]>options.resolution[1]/2-options.resolution[1]/40): # target is behind
            pressKeyLikeHuman(options.keys["backward"])
            releaseKeyLikeHuman(options.keys["forward"])
            print("walking backward")

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

            

class TurnToTargetAction(Action):

    cooldown = 0.05
    lastTimeUsed = 0

    def __init__(self,state):
        super().__init__()
        self.duration = 0.3
        state.lock.acquire()
        if(len(state.enemies)>state.targetedEnemy):
            self.targetLocation = state.enemies[state.targetedEnemy]
        else:
            self.targetLocation = (-1,-1) # the enemy is gone now
        state.lock.release()

    def doAction(self,options,state):
        if(not self.targetLocation[0]==-1): # we actually have a target
            k = PyKeyboard()
            angle = angleFromCenter(self.targetLocation,options)
            if(abs(angle)>1.1):
                if(angle>0):

                    k.release_key(k.right_key)
                    k.press_key(k.left_key)
                    time.sleep(0.05)
                    k.release_key(k.left_key)
                else:
                    k.release_key(k.left_key)
                    k.press_key(k.right_key)
                    time.sleep(0.05)
                    k.release_key(k.right_key)
                
            else:
                k.release_key(k.left_key)
                k.release_key(k.right_key)


    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed


        




class StopExploringAction(Action):

    cooldown = 2
    lastTimeUsed = 0

    def __init__(self,prob=1):
        super().__init__()
        self.probability = prob
        
        
    def doAction(self,options,state):
        releaseKeyLikeHuman(options.keys["forward"])
        releaseKeyLikeHuman(options.keys["backward"])
        releaseKeyLikeHuman(k.right_key)
        releaseKeyLikeHuman(k.left_key)

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed



class SelectTargetAction(Action):
    
    cooldown = 2
    lastTimeUsed = 0

    def __init__(self):
        super().__init__()
        pass
        
    def doAction(self,options,state):
        # m = PyMouse()
        # pyautogui.moveTo(int(self.target[0])-random.randint(0,50),int(self.target[1])+random.randint(0,20))
        # pyautogui.mouseDown()
        # time.sleep(random.random())
        # pyautogui.mouseUp()
        # m.move(int(self.target[0])+random.randint(10,50),int(self.target[1])+random.randint(10,50))
        # m.click(int(self.target[0])+random.randint(10,50),int(self.target[1])+random.randint(10,50),1)
        k.tap_key(k.tab_key)

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed


class ResetViewAction(Action):
    
    cooldown = 5
    lastTimeUsed = 0

    def __init__(self,prob):
        super().__init__()
        self.probability = prob
        pass
        
    def doAction(self,options,state):
        k.tap_key(k.numpad_keys[2])

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed

class FeedPetAction(Action):
    
    cooldown = 50
    lastTimeUsed = 0

    def __init__(self,prob):
        super().__init__()
        self.probability = prob
        pass
        
    def doAction(self,options,state):
        k.tap_key(k.numpad_keys[0])

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed


class LookAroundAction(Action):

    cooldown = 2
    lastTimeUsed = 0

    def __init__(self,prob):
        super().__init__()
        self.probability = prob
        
    def doAction(self,options,state):
        if(random.random()>0):
            k.press_key(k.left_key)
            time.sleep(random.random()/4)
            k.release_key(k.left_key)
        else:
            k.press_key(k.right_key)
            time.sleep(random.random()/4)
            k.release_key(k.right_key)

    @classmethod
    def setLastTimeUsed(cls,timeUsed):
        cls.lastTimeUsed = timeUsed

    @classmethod
    def getLastTimeUsed(cls):
        return cls.lastTimeUsed





#class targetEnemy(Action):


import threading

class GameState:

    loggedIn = 1
    hpPerc = 100
    enemies = []
    targetedEnemy = 0
    lock = threading.Lock()

    def __init__(self):
        self.turning = 0
        self.walking = 0
        self.mouseBlocked = 0
        self.haveTarget = 0
        self.targetType = 0
        self.mainMenuOpen = 0
import math
import random
import time
from _thread import start_new_thread

import pyautogui
from pykeyboard import PyKeyboard
from pymouse import PyMouse

import options as opt
from actions import *
from recognition import *
from state import *

k = PyKeyboard()

options = opt.Options()
options.keys["autoAttack"] = "1"
options.keys["forward"] = "w"
options.keys["backward"] = "s"
options.keys["strafeleft"] = "a"
options.keys["straferight"] = "d"
options.keys["turnleft"] = "left"
options.keys["turnright"] = "right"
options.keys["strike"] = "4"
options.keys["close"] = "q"
options.keys["jump"] = k.space
options.keys["dot"] = "z"

state = GameState()

im1 = pyautogui.screenshot()

pixcolor = im1.getpixel((100, 200))




if(pixcolor == (37,37,38)):
    print("yes")

# print(pixcolor)
# moveMouseSmooth(200,200,1)
# time.sleep(10)
# print(options.resolution)
# actionList = []
# time.sleep(1)
# turnHorizontaly(10)
# time.sleep(1)
# turnHorizontaly(-10)
# # time.sleep(1)
# # turnVerticaly(150)a
# # time.sleep(1)
# # turnVerticaly(-150)

#randomWorldActions = [StopExploringAction(0.0001),WalkAction(0.01),LookAroundAction(0.01)]
# randomWorldActions = [JumpAction(0.0001),LootAction(0.005),LookAroundAction(0.001),WalkAction(0.001)]
randomWorldActions = [LootAction(0.005),LookAroundAction(0.001),FeedPetAction(0.00003)]

state.screen = pyautogui.screenshot()
state.screenReady = 1
state.screenshotThreadRunning = 0
state.recognitionThreadRunning = 0
idxf = 0
start = time.time()
iter = 0
while True:
    iter +=1
    #pyautogui.screenshot()
    start = time.time()
    actionList = []
    if(state.screenReady and not state.recognitionThreadRunning):
        state.recognitionThreadRunning = 1
        state.screenReady = 0   
        start_new_thread(analyzeScreen,(state.screen,options,state))
        if iter % 100 == 0:
            print("saving state screen")
            image = cv2.cvtColor(np.array(state.screen), cv2.COLOR_RGB2BGR) 
            cv2.imwrite("C:/Users/alblcene/Dropbox/wowbot/state.jpg",image) 
            print("state screen saved")
        #print("starting recognition thread")w2
    end = time.time()
    
    #print(state.enemies)
    if(not state.screenReady and not state.screenshotThreadRunning):
        state.screenshotThreadRunning = 1
        start_new_thread(takeScreenShotToState,(state,))
        #print("starting screenshot thread")    
    enemies = state.enemies
    actionList.append(ResetViewAction(0.01))
    
    
    if(state.haveTarget==0 or state.targetType == 0):
        print("dont have a target")
        actionList.append(SelectTargetAction())
        for action in randomWorldActions:
            actionList.append(action)       
        if len(enemies) > 0:    
                actionList.append(TurnToTargetAction(state))
    else:
        if state.targetType == 3 or state.targetType == 2:
            if len(enemies) > 0:    
                actionList.append(TurnToTargetAction(state))    
                print("attacking")
            #actionList.append(WalkToTargetAction(enemies[0]))
                dist = distFromCenter(enemies[0],options)   
                if(dist<150):
                    print("enemy close")
                    actionList.append(StrikeCloseAttackAction())
                else:   
                    print("enemy far")
                    # actionList.append(PetAttackAction())
                    actionList.append(DotAttackAction())    
                    actionList.append(StrikeAttackAction()) 
                    actionList.append(StrikeCloseAttackAction())
            else:
                print("enemy far")  
                actionList.append(LootAction(0.005))
                # actionList.append(PetAttackAction())
                actionList.append(SelectTargetAction())
                actionList.append(LookAroundAction(0.001))  
                actionList.append(DotAttackAction())
                actionList.append(StrikeAttackAction())
        if state.targetType == 1:
            actionList.append(CancelTargetAction(1))
        
        
    if(state.mainMenuOpen):
        actionList.append(CancelTargetAction(1))
    for action in actionList:
        roll = random.random()
        timeSinceLastUse = time.time()-action.getLastTimeUsed()
        if(timeSinceLastUse > action.cooldown and roll<action.probability):
            action.setLastTimeUsed(time.time())
            start_new_thread(action.doAction,(options,state))   
            print(action)
    #print("Main loop time: %f" % (end - start))
    time.sleep(0.01)
    idxf = idxf+1
    
import pyautogui
import cv2
import numpy as np
import statistics
import time
import threading

def analyzeScreen(screen,options,state):
    size = screen.size
    
    if(len(state.enemies)>0):
        print("see enemies")
        enemy = state.enemies[state.targetedEnemy]
        area = (enemy[0]-options.resolution[0]/10, enemy[1]-options.resolution[0]/10, enemy[0]+options.resolution[0]/10, enemy[1]+options.resolution[0]/10)
        offset = (area[0],area[1])
    else:
        print("dont see enemies")
        area = (options.resolution[0]/6, options.resolution[1]/8, options.resolution[0]-options.resolution[0]/6, options.resolution[1]-options.resolution[1]/8)
        offset = (area[0],area[1])
    cropped_img = screen.crop(area)
    start = time.time()
    image = cv2.cvtColor(np.array(cropped_img), cv2.COLOR_RGB2BGR)
    # cv2.imwrite("log/imgs/%d.jpg" % start,image)
    methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
    template = options.enemyHealthYellowTemplate
    # cv2.imwrite("log/imgs/%d_template.jpg" % start,template)

    res = cv2.matchTemplate(template,image,eval(methods[5]))
    end = time.time()
    
    #print("matching time: %f" % (end - start))
    thr1 = 0.02
    loc = np.where( res < thr1)
    x = []
    y = []
    enemies = []
    start = time.time()
    last_loc = (0,0)
    for pt in zip(*loc[::-1]):
        # pt = (pt[0]+offset[0]+options.resolution[0]/20,pt[1]+offset[1]+options.resolution[1]/200)
        pt = (pt[0]+offset[0]+100,pt[1]+offset[1]+100)
        if(abs(pt[0]-last_loc[0])>options.resolution[0]/10 or abs(pt[1]-last_loc[1])>options.resolution[1]/100):
            enemies.append(pt)
            last_loc = pt
    area = (options.resolution[0]/5.4,options.resolution[1]/40,options.resolution[0]/5.4+options.resolution[0]/160,options.resolution[1]/40+options.resolution[1]/30)
    cropped_img = screen.crop(area)
    image = cv2.cvtColor(np.array(cropped_img), cv2.COLOR_RGB2BGR)
    # cv2.imwrite("log/imgs/%d_crop.jpg" % start,image)
    template = options.enemyBar
    # cv2.imwrite("log/imgs/%d_crop_template.jpg" % start,template)
    res = cv2.matchTemplate(template,image,eval(methods[5]))
    thr2 = 0.05
    loc = np.where( res < thr2)
    # print(state.haveTarget)
    state.lock.acquire()
    if(loc[0].size>0):
        state.haveTarget = 1
        crop = np.array(cropped_img)
        if crop[20,10,0] > 100 and crop[20,10,1] < 100 and crop[20,10,2] < 100: 
            state.targetType = 3
        elif crop[20,10,0] > 100 and crop[20,10,1] > 100 and crop[20,10,2] < 100: 
            state.targetType = 2
        elif crop[20,10,0] < 100 and crop[20,10,1] < 100 and crop[20,10,2] > 100: 
            state.targetType = 1
        else:
            state.targetType = 0
        print("have target")
    else:
        state.haveTarget = 0
        state.targetType = 0
        print("no target")
    state.lock.release()

    #Look for main menu
    area = (options.resolution[0]/2-options.resolution[0]/25,options.resolution[1]/2.8-options.resolution[1]/50,options.resolution[0]/2,options.resolution[1]/2.8+options.resolution[1]/50)
    cropped_img = screen.crop(area)
    image = cv2.cvtColor(np.array(cropped_img), cv2.COLOR_RGB2BGR)

    template = options.mainMenu
    # cv2.imwrite("log/imgs/%d_crop_template.jpg" % start,template)
    res = cv2.matchTemplate(template,image,eval(methods[5]))
    thr2 = 0.02
    loc = np.where( res < thr2)
    # print(state.haveTarget)
    state.lock.acquire()
    if(loc[0].size>0):
        state.mainMenuOpen = 1
        print("have target")
    else:
        state.mainMenuOpen = 0
        print("no target")
        
    state.enemies = enemies
    state.lock.release()
    end = time.time()
    #print("postproc time: %f" % (end - start))
    state.recognitionThreadRunning = 0
    # for i in range(size[0]):
    #     for j in range(size[1]):
    #         pix = screen.getpixel((i,j))
    #         if(pix==(191,187,0)):
    #             state.enemies[-1] = (i,j) 

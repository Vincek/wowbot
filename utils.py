import math
import pyautogui

def twoPointsDist(pt1,pt2):
    return math.sqrt((pt1[0]-pt2[0])*(pt1[0]-pt2[0])+(pt1[1]-pt2[1])*(pt1[1]-pt2[1]))

def distFromCenter(pt,options):
    return math.sqrt((pt[0]-options.resolution[0]/2)*(pt[0]-options.resolution[0]/2)+(pt[1]-options.resolution[1]/2)*(pt[1]-options.resolution[1]/2))

def angleFromCenter(pt,options):
    x = -pt[1]+options.resolution[1]/2
    y = -pt[0]+options.resolution[0]/2
    print("angle coords %f %f" % (x,y))
    return math.atan2(y,x)

def takeScreenShotToState(state):
    state.screen = pyautogui.screenshot()
    state.screenReady = 1
    state.screenshotThreadRunning = 0

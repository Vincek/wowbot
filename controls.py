import pyautogui
import random
import time
from pymouse import PyMouse
from pykeyboard import PyKeyboard

k = PyKeyboard()


def pressKeyLikeHuman(key):
    k = PyKeyboard()
    time.sleep(random.randint(50,100)/1000)
    k.press_key(key)
    random.randint(20,50)/1000
    k.release_key(key)

def holdKeyLikeHuman(key):
    k = PyKeyboard()
    time.sleep(random.randint(50,100)/1000)
    k.press_key(key)

def releaseKeyLikeHuman(key):
    k = PyKeyboard()
    time.sleep(random.randint(50,100)/1000)
    k.release_key(key)


def moveMouseSmooth(pixelsX,pixelsY,duration):
    m = PyMouse()
    deltaX = pixelsX/100
    deltaY = pixelsY/100
    inc = 0
    delay = duration/100
    for i in range(1,100):
        inc = inc+1
        pos = m.position()
        if(abs(inc*deltaX)>1):
            m.move(pos[0]+round(inc*deltaX),pos[1])
            inc = 0
        if(abs(inc*deltaY)>1):
            m.move(pos[0],pos[1]+round(inc*deltaY))
            inc = 0
        time.sleep(delay)




    